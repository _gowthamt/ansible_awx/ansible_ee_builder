# Ansible Execution Environment: Netbox Inventory
# Jan 31, 2022

[![N|Solid](https://netbox.readthedocs.io/en/stable/netbox_logo.svg)](https://netbox.readthedocs.io/)
Original Project https://github.com/cdot65/ansible-ee-netbox-inventory.git

## Overview

This project aims to sync Netbox inventory into Ansible AWX through the construct of an Execution Environment.

## Contacts
Gowtham Tamilselvan (gtamilse@cisco.com)

### Prep your Python environment

Perform following install steps inside a python virtual environment

If using Poetry:
Refer to the Poetry Lock file located at [poetry.lock](poetry.lock) for detailed descriptions on each package installed.
1. install Python dependencies
```bash
poetry install
```
2. activate environment
```bash
poetry shell
```

If using venv:
1. Create virtual environment
```
python -m venv venv/
```
2. Activate virtual environment
```
source venv/bin/activate
```
3. Install Ansible-Builder inside the environment
```
pip install ansible-builder
```


## Executing the build

Ansible Builder can be utilized to create the basic Dockerfile needed to create EE container.

1. Modify the input files
- Ansible Builder uses the execution-environment.yml file to create the Dockerfile, ensure this file is correct.
- requirements.txt - add any python pip pkgs that are needed to be installed inside the EE container
- requirements.yml - add any ansible collections that are needed to be installed inside the EE container

2. Run Ansible Builder to build the container image (or at the very list build the Dockerfile)

```
ansible-builder build --tag (please insert the name of your container image here)

ex: ansible-builder build -t containers.cisco.com/cx-ttg-sre/awx-netbox-ee:latest
```

3. Ansible builder will create a context dir with _build folder and the Dockerfile used to create the container.

```
(venvglobal) ansible-ee-sre gtamilse$ tree context/
context/
├── Dockerfile
└── _build
    ├── requirements.txt
    └── requirements.yml

1 directory, 3 files
```

4. Verify the container is built

```
(venvglobal) ansible-ee-sre gtamilse$ docker image list
REPOSITORY                                                            TAG          IMAGE ID       CREATED          SIZE
containers.cisco.com/gtamilse/awx-netbox-ee                           20220127     187a384924d7   4 days ago       863MB
containers.cisco.com/gtamilse/awx-netbox-ee                           latest       187a384924d7   4 days ago       863MB
```

## Upload container image

Your last step will be to upload your resulting Docker container image to a centralized repository that Ansible AWX can access.

1. Publish Container to containers.cisco.com

```
(venvglobal) ansible-ee-sre gtamilse$ docker login <> containers.cisco.com
(venvglobal) ansible-ee-sre gtamilse$ docker push containers.cisco.com/gtamilse/awx-sre-ee:latest
```
2. Setup Execution Environment inside AWX to utilize this container for projects!

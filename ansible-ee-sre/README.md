# Ansible Execution Environment: SRE Python and Ansible Env
# Jan 31, 2022

## Overview

This project aims to build an Ansible Execution Environment suitable for running python scripts and ansible playbooks inside AWX.

## Contacts
Gowtham Tamilselvan (gtamilse@cisco.com)

### Prep your Python environment

Perform following install steps inside a python virtual environment

If using Poetry:
1. install Python dependencies

```bash
poetry install
```

2. activate environment

```bash
poetry shell
```

If using venv:
1. Create virtual environment

```
python -m venv venv/
```
2. Activate virtual environment

```
source venv/bin/activate
```

3. Install Ansible-Builder inside the environment

```
pip install ansible-builder
```


## Executing the build

Ansible Builder can be utilized to create the basic Dockerfile needed to create EE container.

1. Modify the input files
- Ansible Builder uses the execution-environment.yml file to create the Dockerfile, ensure this file is correct.
- requirements.txt - add any python pip pkgs that are needed to be installed inside the EE container
- requirements.yml - add any ansible collections that are needed to be installed inside the EE container

2. Run Ansible Builder to build the container image (or at the very list build the Dockerfile)

DO NOT RUN FOR THIS PROJECT!!
```
ansible-builder build --tag (please insert the name of your container image here)
```

3. Ansible builder will create a context dir with _build folder and the Dockerfile used to create the container

```
(venvglobal) ansible-ee-sre gtamilse$ tree context/
context/
├── Dockerfile
└── _build
    ├── requirements.txt
    └── requirements.yml

1 directory, 3 files
```
4. Modify the Dockerfile as per you need (in our case to install Sastre-Pro and Sastre-Ansible)

```
Check Dockerfile inside contexts dir
```
5. Run the Docker build manually

```
(venvglobal) ansible-ee-sre gtamilse$ ansible-ee-sre/context/
(venvglobal) ansible-ee-sre gtamilse$ docker build -t containers.cisco.com/gtamilse/awx-sre-ee:20220131 .
(venvglobal) ansible-ee-sre gtamilse$ docker build -t containers.cisco.com/gtamilse/awx-sre-ee:latest .
```
6. Verify the container pkgs

```
(venvglobal) ansible-ee-sre gtamilse$ docker run --rm containers.cisco.com/gtamilse/awx-sre-ee:20220131 python3 --version
Python 3.8.12
(venvglobal) ansible-ee-sre gtamilse$ docker run --rm containers.cisco.com/gtamilse/awx-sre-ee:20220131 sdwan --version
Sastre-Pro Version 1.16.8. Catalog: 73 configuration items, 31 operational
```


## Upload container image

Your last step will be to upload your resulting Docker container image to a centralized repository that Ansible AWX can access.

1. Publish Container to containers.cisco.com

```
(venvglobal) ansible-ee-sre gtamilse$ docker login <> containers.cisco.com
(venvglobal) ansible-ee-sre gtamilse$ docker push containers.cisco.com/gtamilse/awx-sre-ee:20220131
(venvglobal) ansible-ee-sre gtamilse$ docker push containers.cisco.com/gtamilse/awx-sre-ee:latest
```
2. Setup Execution Environment inside AWX to utilize this container for projects!

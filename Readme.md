# Ansible Execution Environment Container Builder
# Jan 31, 2022

## Overview

This project aims to build Ansible AWX Execution Environment Containers.

Check individual directories for instructions.

## Contacts
Gowtham Tamilselvan (gtamilse@cisco.com)

## Directory Structure

Project contains 2 EE container build files:

1) ansible-ee-netbox-inventory - used to build EE container with python netbox pkgs installed so you can retrieve data from netbox inside of AWS to create dynamic inventories

2) ansible-ee-sre - used to build EE container with ansible, python3.8, Sastre-Pro, Sastre-Ansible, and other python pkgs needed to run SRE scripts/playbooks

```
.
├── Jenkinsfile
├── Readme.md
├── ansible-ee-netbox-inventory
│   ├── README.md
│   ├── context
│   │   ├── Dockerfile
│   │   └── _build
│   │       ├── requirements.txt
│   │       └── requirements.yml
│   ├── execution-environment.yml
│   ├── pyproject.toml
│   ├── requirements.txt
│   └── requirements.yml
└── ansible-ee-sre
    ├── README.md
    ├── context
    │   ├── Dockerfile
    │   └── _build
    │       ├── requirements.txt
    │       └── requirements.yml
    ├── execution-environment.yml
    ├── pyproject.toml
    ├── requirements.txt
    └── requirements.yml

6 directories, 18 files
```